/* Library for Bipolar Stepper  Motor with and Without Blocking

 Copyright (C) 2016  Nikolaos Papadakis

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//#define DEBUG
//
//
//
#include "BipolarStepper.h"

/**
 * Main function
 *
 * Function which is performed on each loop.
 * Performs the following:
 *
 * - if the current position is different than the last calls the PerformMovementAsync
 * - if the position is the same then stops the motor.
 *
 */
void BipolarStepper::update() {
	if (_targetPos != _position) {
		if (_motStep == 0) {
			_motStep = _lastPhase;
		}
		PerformMovementAsync();
	} else {
		if (_motStep) {
			_lastPhase = _motStep;
			stop();
			_motStep = 0;
		}
	}

}
;

/*
 Initialises Bipolar Stepper Motor instance
 Inputs:
 - pwmA:  (int) pin number
 - pwmB:  (int) pin number
 - dirA:  (int) pin number
 - dirB:  (int) pin number
 - ResetPion:  (int) pin number
 - StepsPerRevolutions:  (int) Steps required for a full revolution by stepper motor
 */
BipolarStepper::BipolarStepper(int pwmA, int pwmB, int dirA, int dirB,
		int ResetPin, int StepsPerRevolution) {
	_PWMA = pwmA;
	_PWMB = pwmB;
	_DIRA = dirA;
	_DIRB = dirB;
	_motorXResetPin = ResetPin;
	_StepsPerRevolution = StepsPerRevolution;

	pinMode(_PWMA, OUTPUT);
	pinMode(_PWMB, OUTPUT);
	pinMode(_DIRA, OUTPUT);
	pinMode(_DIRB, OUTPUT);

	// State Variable initialisation
	_position = 0;
	_targetPos = 0;
	_lastPhase = 0;
	ConfigSpeed(90);
}
/**
 * Resets current position to @param newPosition
 */
void BipolarStepper::ResetCurrentPos(int newPosition) {
	_position = newPosition;
	_targetPos= newPosition;
}

/**
 * - Calls millis to get new time
 * - If new time - previous movement time > time Delay then change motor step
 * - Calls activate new phase
 *
 * This is performed whenever a motor changes activation stage
 */
void BipolarStepper::PerformMovementAsync() {
	_directionX = (_targetPos > _position);
	currentMillisX = millis();
	//delay till next state
	if (currentMillisX - previousMillisX >= _timeDelay) {
#ifdef DEBUG
		if (_motStep == 4)
		Serial.println(currentMillisX - previousMillisX);
#endif // DEBUG
		previousMillisX = currentMillisX;

		if (_directionX == true) { //X: CCW    for Y is CW
			_motStep++;
			_position++;
			if (_motStep > 4)
				_motStep = 1;
		} else { //CW
			_motStep--;
			_position--;
			if (_motStep < 1)
				_motStep = 4;
		}
		ActivateMotorPhase();
	}
}

/**
 * Method for stopping the Bipolar Stepper motor on it tracks
 *
 * responsible for setting Motstep to zero
 */

/**
 *
 */
int BipolarStepper::stop() {
	digitalWrite(_DIRA, LOW);
	digitalWrite(_DIRB, LOW);
	digitalWrite(_PWMA, LOW);
	digitalWrite(_PWMB, LOW);

	// Set state and position
	_lastPhase = _motStep;
	_motStep = 0;
	_targetPos = _position;
	return 0;
}
/**
 * Method for setting the target position relative to the current target position (NOT FROM current position).
 * Inputs:
 * - StepsFromCurrentTarget:  (int) number of steps from current target position
 */
void BipolarStepper::setTargetPositionRelative(int targetPosition) {
	//_targetPos = targetPosition + _position; 
	_targetPos = targetPosition + _targetPos; // this is more reliable.
}

/**
* Method for setting the target position absolute.
* Inputs:
* - targetPosition:  (int) number of steps from current target position
*/
void BipolarStepper::setTargetPositionAbsolute(int targetPosition) {
	_targetPos = targetPosition;

}

/**Configures speed of motor to achieves Speed[deg/sec]
* @param Speed Rotational Velocity in [deg/sec]
*/
void BipolarStepper::ConfigSpeed(float Speed) {
	// Make sure speed is higher than zero
	if (Speed < 0.1) {
		Speed = 0.1;
	}
	_timeDelay = (360.0 / _StepsPerRevolution) / Speed * 1000.0;
	// _timeDelay has to be positive and non zero
	_timeDelay = max(_timeDelay, 1);
#ifdef DEBUG
	Serial.print("Time delay :");
	Serial.print(_timeDelay);
	Serial.print("   ");
#endif
}

/**
 *	 Function for performing Steps with Delay. Here for compatibility purposes
 */
void BipolarStepper::PerformMultipleStepsWithDelay(int steps, boolean dir) {
	if (steps <= 0) {
		return;
	}
	if (dir) {
		for (int p = 1; p <= steps; p++) {
			ChangeStatesWithDelay(dir);
		}
	} else {
		for (int p = steps; p >= 1; p--) {
			ChangeStatesWithDelay(dir);
		}
	}
}

/**
 * FUNCTION which is called to change the pin polarity.
 *
 * - Changes pin polarity
 *******************************/
int BipolarStepper::ActivateMotorPhase() {

	switch (_motStep) {
	case 1:
		digitalWrite(_DIRA, HIGH);
		digitalWrite(_DIRB, LOW);
		digitalWrite(_PWMA, HIGH);
		digitalWrite(_PWMB, LOW);
		break;

	case 2:
		digitalWrite(_DIRA, LOW);
		digitalWrite(_DIRB, HIGH);
		digitalWrite(_PWMA, LOW);
		digitalWrite(_PWMB, HIGH);
		break;

	case 3:
		digitalWrite(_DIRA, LOW);
		digitalWrite(_DIRB, LOW);
		digitalWrite(_PWMA, HIGH);
		digitalWrite(_PWMB, LOW);
		break;

	case 4:
		digitalWrite(_DIRA, LOW);
		digitalWrite(_DIRB, LOW);
		digitalWrite(_PWMA, LOW);
		digitalWrite(_PWMB, HIGH);
		break;
	}

	return _motStep;
}

/**
 *  Performs sequential state change from one phase of the bipolar to the next.
 */
void BipolarStepper::ChangeStatesWithDelay(boolean dir) {

	if (dir) {
		_motStep = (_motStep + 1);
		if (_motStep > 4) {
			_motStep = 1;
		}
		ActivateMotorPhase();
	} else {
		_motStep--;
		if (_motStep < 1) {
			_motStep = 4;
		}
		ActivateMotorPhase();
	}
	delay(_timeDelay);
}

