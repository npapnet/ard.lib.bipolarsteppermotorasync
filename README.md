# README #

Arduino Non Blocking (Async) Bipolar Stepper motor driver for the Arduino Shield R3.

This repository contains two source files:

a) _BipolarStepper.h_ header file

b) _BipolarStepper.cpp_ cpp file

And an example file. 

# What is this repository for? #

* Quick summary: Library for a non blocking Bipolar Stepper Motor driver

* Version: v2.1 Not compatible with 1.9.2

## What is this repository for? ##

This is a repository for driving a bipolar stepper motor using Arduino Motro Shield R3.
The driver can easily be extened to other shields, provided that one knows how to change the _fase_ method.

## API 

- BipolarStepper(int pwmA, int pwmB, int dirA, int dirB, int ResetPin, int StepsPerRevolution)(#api_init)

- void update()(#api_update)

- void stop()(#api_stop)

- void PerformMultipleStepsWithDelay(int steps, boolean dir)](#api_PerformMultipleStepsWithDelay)

- void setTargetPositionRelative(int StepsFromCurrentTarget)](#api_setTargetPositionRelative)

- void setTargetPositionAbsolute(int targetPosition)](#api_setTargetPositionAbsolute)

- void ConfigSpeet(float Speed)](#api_ConfigSpeed)

- void ResetCurrentPosition(int targetPosition)](#api_ResetCurrentPosition)

- int reportCurrentPosition()](#api_reportCurrentPosition)


### <a id="api_init"></a> __void init(int pwmA, int pwmB, int dirA, int dirB, int ResetPin, int StepsPerRevolution)__ :  

Initialises Bipolar Stepper Motor instance
	 
	 Inputs:
	 
	 - pwmA:  (int) pin number
	 
	 - pwmB:  (int) pin number
	 
	 - dirA:  (int) pin number
	 
	 - dirB:  (int) pin number
	 
	 - ResetPion:  (int) pin number

	 - StepsPerRevolutions:  (int) Steps required for a full revolution by stepper motor


### <a id="api_update"></a>	__void update()__:  

Main function for async control of bipolar motor 

This function should be performed on every loop cycle. It checks if there is need to perform change of state of the bipolar motor. Uses *millis()*.
	 
### <a id="api_stop"></a> __int stop()__: 

Method for stopping the Bipolar Stepper motor.

This method should be performed in case of:

- the motor reaching its target

- emergency stop.
	
### <a id="api_PerformMultipleStepsWithDelay"></a> __void PerformMultipleStepsWithDelay(int passi, boolean dir)__:  

Function for performing Steps with Delay. 

This function is here for compatibility purposes.
	
### <a id="api_setTargetPositionRelative"></a> __void setTargetPositionRelative(int StepsFromCurrentTarget)__:  

Method for setting the target position relative to the current target position (NOT FROM current position).
	
	Inputs:
	
	- StepsFromCurrentTarget:  (int) number of steps from current target position
	
### <a id="api_setTargetPositionAbsolute"></a> __void setTargetPositionAbsolute(int targetPosition)__:  

Method for setting the target position absolute.
	 
	 Inputs:
	 
	 - targetPosition:  (int) target position
	 
	 
### <a id="api_ConfigSpeed"></a> __void ConfigSpeed(float Speed)__: 

Configures speed of motor to achieves Speed[deg/sec]

	- Speed: Rotational Velocity in [deg/sec]
	
### <a id="api_ResetCurrentPos"></a> __void ResetCurrentPos(int currentPosition)__: 

Reset current position 

### <a id="api_reportCurrentPosition"></a> __int reportCurrentPosition()__: 

Reports current position in Steps
	


# How do I get set up? 

* Required Hardware
    * [ArduinoMega](https://www.arduino.cc/en/Main/ArduinoBoardMega2560)
	* [Arduino Motor Shield R3](https://www.arduino.cc/en/Main/ArduinoMotorShieldR3)
	* A bipolar stepper motor

* Summary of set up 

    - Find the arduino Libraries folder (under __windows 10__ it should be in Documents\Arduino\libraries)
    - Create a directory called __BipolarStepper__
    - Clone this repo in the Libraries folder in  __BipolarStepper__ folder .
    
    Optional: 
    
    - open Arduino and open the example  _SimpleNonBlockingBipolar_ 
     
* Configuration:
* Dependencies
	* Arduino IDE 1.6.7
	
* Database configuration
* How to run tests
* Deployment instructions


# HISTORY 

## TODO 

- add config Speed Steps per second in Bipolar Stepper
- add getStepPerRevolution in Bipolar Stepper

- update example
 
- Add reset current position

## 2016-06-01: v2.1

- Made changes to update function (stop is only called once). 

- changing _motstep to zero occurs in stop().

- Remembering the last phase is a responsibility of stop().

## 2016-06-01: v2.0

- updated example to V2.0

- renamed phase

- Bug fix: When performing movements not multiples of 4 the last state of the motor is not in sequence with the other motor. Try moving relative two and then relative 1 step.  

- Removed Init and creating constructors.

- Renaming class from BipolarStepperClass to BipolarStepper

- Started Version 2.0

## 2016-??

This project started on while experimenting with  [Arduino Motor Shield R3](https://www.arduino.cc/en/Main/ArduinoMotorShieldR3).

There are a lot of tutorials for DC motors, with Arduino Motor Shield R3, but not enough for bipolar steppers (or steppers). 

I used as starting point a tutorial by [Mauro Alfieri](http://www.mauroalfieri.it/elettronica/motor-shield-r3-arduino.html). However the problem was that it used the delay() so it was blocking.

I looked for other libraries but couldn't find any for non-blocking and I thought, let's make one as a learning experience. And here it is. 

### Who do I talk to? ###

* Repo owner or admin: npapnet@gmail.com
* Other community or team contact