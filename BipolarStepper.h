/* Library for Bipolar Stepper  Motor with and Without Blocking

 Copyright (C) 2016  Nikolaos Papadakis

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
// BipolarStepper.h
#ifndef _BIPOLARSTEPPER_h
#define _BIPOLARSTEPPER_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

class BipolarStepper {
protected:

public:
	BipolarStepper(int pwmA, int pwmB, int dirA, int dirB, int ResetPin,
			int StepsPerRevolution);
	//BipolarStepper();
	//~BipolarStepper();


	void update();
	void PerformMultipleStepsWithDelay(int passi, boolean dir);
	int stop();
	void setTargetPositionRelative(int StepsFromCurrentTarget);

	void setTargetPositionAbsolute(int targetPosition);

	void ConfigSpeed(float Speed);
	void ResetCurrentPos(int newPosition); // Consider using default argument i.e. void ResetCurrentPos(int newPosition=0);

	/**
	 * Reports current position in Steps</summary>
	 */
	int reportCurrentPosition() 	{
		return _position;
	}

	int getTargetPos() const {
		return _targetPos;
	}

	int getStepsPerRevolution() const {
		return _StepsPerRevolution;
	}

private:
	void ChangeStatesWithDelay(boolean dir);
	int ActivateMotorPhase();
	void PerformMovementAsync();

	//X motor pins
	int _PWMA;
	int _PWMB;
	int _DIRA;
	int _DIRB;
	int _StepsPerRevolution = 200;
	int _motorXResetPin;    //motor X optical reset pin

	unsigned long previousMillisX;
	unsigned long currentMillisX;

	/**
	 * stores the last phase before stopping. So when the motor starts again the movement is not erratic.
	 */
	int _lastPhase;
	int _motStep;           //current cycle position of motor1 (range 0-3)
	int _position = 0;       //current angle on X axis (steps)

	//=================positioning system variables======================
	boolean _directionX = true;              //rotation direction for motor X
	int _targetPos;          //target position on X axis (steps)
	unsigned long _timeDelay = 20;          // Time between changes

	float _minDegStep; /**minimum degree change per step on X*/
	boolean calibrateComplete = false;  //calibration for motor X complete
};

#endif
