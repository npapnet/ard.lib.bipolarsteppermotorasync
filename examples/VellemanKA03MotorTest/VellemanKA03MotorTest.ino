#include "Arduino.h"
/* This program is part of an example for StepperMotorAsync.h, and StepperMotorAsync.cpp libraries

	Copyright(C) 2016 N.Papadakis

	This program is free software; you can redistribute it and / or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110 - 1301  USA
*/

/**********************************************************/
#include <BipolarStepper.h>
void moveWithoutDelay();  //prototype

// Velleman KA03 pinout 
#define PWMA 3
#define DIRA 2
#define PWMB 11
#define DIRB 13

/**********************************************************/
BipolarStepper myMotor(PWMA, PWMB, DIRA, DIRB, 0, 200); //v.2.0. initialisation
//BipolarStepperClass myMotor; //v.1.9.2. initialisation

int currentWaypoint = 0; // no of steps
float currentSpeed = 90; // [deg/sec]
int ticToc;
int dt;

/**********************************************************/

void setup() {
	//myMotor.init(PWMA, PWMB, DIRA, DIRB, 0, 200);

	Serial.begin(9600);
	Serial.println("==================================================================================");
	Serial.println(" Arduino Motor Shield R3 , Non Blocking (Async) Bipolar Stepper Motor  ");
	Serial.println(" Copyright(C) 2016 N. Papadakis\n");
	Serial.println(" This example comes with ABSOLUTELY NO WARRANTY");
	Serial.println(" This is free software, and you are welcome to redistribute it");
	Serial.println(" under certain conditions");
	Serial.println(" ");
	Serial.println("  You should have received a copy of the GNU General Public License");
	Serial.println("  along with this program; if not, write to the Free Software");
	Serial.println("  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110 - 1301  USA");
	Serial.println("===================================Starting======================================");

	myMotor.ConfigSpeed(currentSpeed);
	ticToc = millis();
}



/**********************************************************/

void loop() {

	moveWithoutDelay();
	//MoveWithDelay();
	//delay(1000);
}


/*
* This is a function which uses the blocking sets of the functions.
*/
void MoveWithDelay()
{
	Serial.println("New Loop");
	ticToc = millis();
	myMotor.ConfigSpeed(90);
	myMotor.PerformMultipleStepsWithDelay(100, true);
	dt = millis() - ticToc;
	Serial.print("  dt:");
	Serial.println(dt);
	myMotor.stop();
	/*
	delay(1000);
	myMotor.PerformMultipleStepsWithDelay(100, true);
	myMotor.stop();
	delay(1000);
	Serial.println("Should go CW");
	myMotor.PerformMultipleStepsWithDelay(200, false);
	myMotor.stop();
	delay(3000);*/
}


/*
* This function performs a series of NON Blocking movements
*  - Uses previously set speed
*  - Goes through different waypoints.
*		-Starting from zero steps does 50 steps(equivalent to 90[deg]), with a speed of 90[deg/sec]. The length of the motion should be about 1000[ms]
*		- once it arrives at a way point it delays (blocking) for a second and then it sets another waypoint.
*/
void moveWithoutDelay()
{
	if (currentWaypoint <= myMotor.reportCurrentPosition() && currentWaypoint < 400)
	{
		dt = millis() - ticToc;
#ifdef DEBUG
		Serial.print("  dt:");
		Serial.println(dt);
		Serial.print("Reached Waypoint:");
#endif // DEBUG
		Serial.println(currentWaypoint);
		delay(1000);
		currentWaypoint += 50;
		//currentSpeed += 10;
		//myMotor.ConfigSpeed(currentSpeed);
		myMotor.setTargetPositionRelative(currentWaypoint);
		ticToc = millis();
	}
	else
	{
		myMotor.update();
	}
}
